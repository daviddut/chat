Template.commentItem.helpers({

    submittedText: function () {
        return this.submitted.toString();
    },

    isReply: function () {
        return Session.get("replyId") == this._id;
    },

    childComments: function () {
        // return only child comments
        return Comments.find({parentCommentId: this._id });
    }
});

Template.commentItem.events({
    'click .toggle-reply': function () {
        Session.set("replyId", this._id);
    }
});
