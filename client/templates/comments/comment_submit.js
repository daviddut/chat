Template.commentSubmit.created = function () {
    Session.set('commentSubmitErrors', {});
}

Template.commentSubmit.helpers({
    errorMessage: function (field) {
        return Session.get('commentSubmitErrors')[field];
    },
    errorClass: function (field) {
        return !!Session.get('commentSubmitErrors')[field] ? 'has-error' : '';
    }
});

Template.commentSubmit.events({
    'submit form': function (e, template) {
        e.preventDefault();
        var $body = $(e.target).find('[name=comment-body]');
        processComment(template,$body); 
    },
    
    'keypress textarea': function (e, template) {  
        if (event.charCode == 13 && !e.shiftKey) {
            e.preventDefault();   
            var $body = $(e.target);
            processComment(template,$body);                  
        } else if (event.charCode == 13 && e.shiftKey)  {
            //add line break to text
        }
    }  
});

function processComment(template, $body) {
        var id;
        var parentCommentId = '';

        //if we add a new comment to the main thread
        if (template.data.parentCommentId == null) {
            id = template.data._id;
            parentCommentId = '';
        }

        var comment = {
            body: $body.val(),
            postId: id,
            parentCommentId: parentCommentId
        };

        var errors = {};
        if (!comment.body) {
            errors.body = "Please write some content";
            return Session.set('commentSubmitErrors', errors);
        } else {
            if (parentCommentId != '') {
                Session.set("replyId", '');
            }
        }

        Meteor.call('commentInsert', comment,
            function (error) {
                if (error) {
                    throwError(error.reason);
                } else {
                    $body.val('');
                }
            }
        );   
};