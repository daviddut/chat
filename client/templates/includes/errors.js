
//Code taken from the book Discover Meteor - Building Real-Time JavaScript Web Apps p.132
Template.errors.helpers({
    errors: function () {
        return Errors.find();
    }
});


Template.error.rendered = function () {
    var error = this.data;
    Meteor.setTimeout(function () {
        Errors.remove(error._id);
    }, 3000);
};
