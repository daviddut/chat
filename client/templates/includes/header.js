Template.header.helpers({

    currentlyOpenedDiscussion: function () {
        var currentDiscussion = Session.get("currentDiscussion")

        if (currentDiscussion == null || currentDiscussion == '')
            return false;

        return '/posts/' + currentDiscussion;
    }
});

Template.header.events({

    'submit form': function (e) {
        e.preventDefault();
        var year = $(e.target).find('[name=filterByYear]').val();
        Session.set("currentYear", year);
    }
});
