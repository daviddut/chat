Template.postPage.helpers({

    //Returns parent node comments for the current post page
    comments: function () {
        return Comments.find({
            parentCommentId: '',
            postId: this._id
        });
    }

});
