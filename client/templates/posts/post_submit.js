Template.postSubmit.events({
    'submit form': function (event) {
        event.preventDefault(); //prevent submit button from submitting the form

        var imageId = Session.get("idImage");
        
        
        if (imageId == undefined){
            imageId = 'none';
        } else {
           imageId = imageId._id
        }

        var post = {
            material: $(event.target).find('[name=material]').val(),
            title: $(event.target).find('[name=title]').val(),
            imageId: imageId
        };

        var errors = validatePost(post);
        var filename = '';

        if (errors.title || errors.material)
            return Session.set('postSubmitErrors', errors);

        Meteor.call('postInsert', post, function (error, result) {
            // display the error to the user and abort
            if (error)
                return throwError(error.reason);

            // show this result but route anyway
            if (result.postExists)
                throwError('This link has already been posted');

            Router.go('postPage', {
                _id: result._id
            });
        });

        Session.set("idImage", 'none');
    },

    'change .myFileInput': function (event, template) {

        FS.Utility.eachFile(event, function (file) {
            var idImage = Images.insert(file, function (err, fileObj) {
                this.filename = file.fileName;

                if (err) {
                    // handle error
                } else {
                    // handle success depending what you need to do
                    var userId = Meteor.userId();
                    var imagesURL = {
                        'postSubmit.image': '/cfs/files/images/' + fileObj._id
                    };
                    Meteor.users.update(userId, {
                        $set: imagesURL
                    });
                }
            });

            Session.set("idImage", idImage);
        });
    }
});


Template.postSubmit.created = function () {
    Session.set('postSubmitErrors', {});
}

Template.postSubmit.helpers({
    errorMessage: function (field) {
        return Session.get('postSubmitErrors')[field];
    },
    errorClass: function (field) {
        return !!Session.get('postSubmitErrors')[field] ? 'has-error' : '';
    }
});
