Template.postItem.helpers({
    ownPost: function () {
        return this.userId === Meteor.userId();
    },

    images: function () {
        // Where Images is an FS.Collection instance
        return Images.findOne({
            _id: this.imageId
        })
    },

    monthAndYear: function () {
        var arr = this.submitted.toString().split(' ');
        return arr[1] + ' ' + arr[3];
    }

});
