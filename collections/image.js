Images = new FS.Collection("images", {
    stores: [
      new FS.Store.GridFS("images"),
      new FS.Store.GridFS("thumbs", {
            transformWrite: function (fileObj, readStream, writeStream) {
                // Transform the image into a 10x10px thumbnail
                gm(readStream, fileObj.name()).resize('40', '40').stream().pipe(writeStream);
            }
        })
    ],
    filter: {
        allow: {
            contentTypes: ['image/*'] //allow only images in this FS.Collection
        }
    }
});


Images.deny({
    insert: function () {
        return false;
    },
    update: function () {
        return false;
    },
    remove: function () {
        return false;
    },
    download: function () {
        return false;
    }
});

Images.allow({
    insert: function () {
        return true;
    },
    update: function () {
        return true;
    },
    remove: function () {
        return true;
    },
    download: function () {
        return true;
    }
});
