Posts = new Meteor.Collection('posts');

Posts.allow({
    update: function (userId, post) {
        return ownsDocument(userId, post);
    },
    remove: function (userId, post) {
        return ownsDocument(userId, post);
    },
});

Posts.deny({
  update: function(userId, post, fieldNames, modifier) {
    var errors = validatePost(modifier.$set);
    return errors.title || errors.material;
  }
});

Posts.deny({
    update: function (userId, post, fieldNames) {
        // may only edit the following two fields:
        return (_.without(fieldNames, 'material', 'title').length > 0);
    }
});

Meteor.methods({
    postInsert: function (postAttributes) {
        check(Meteor.userId(), String);
        check(postAttributes, {
            title: String,
            material: String,
            imageId: String
        });

        var errors = validatePost(postAttributes);
        if (errors.title || errors.material || errors.imageId && post.imageId != "none")
            throw new Meteor.Error('invalid-post', "You must set a title, choose an image and fill the material field for your discussion");

        var user = Meteor.user();
        var post = _.extend(postAttributes, {
            userId: user._id,
            author: user.username,
            submitted: new Date(),
            commentsCount: 0
        });

        var postId = Posts.insert(post);

        return {
            _id: postId
        };
    }
});


validatePost = function (post) {
    var errors = {};

    if (!post.title)
        errors.title = "Please fill in a headline";

    if (!post.material)
        errors.material = "Please fill the material field";
        
    if (!post.imageId || post.imageId == "none" )
        errors.imageId = "Please choose an image";   
        
    return errors;
}


/** Commented since meteor methods bypass allow block
//Allows posting if user is logged in
Posts.allow({
  insert: function(userId, doc) {
    // only allow posting if you are logged in
    return !! userId;
  }
});*/
