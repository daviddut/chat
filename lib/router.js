Router.configure({
    layoutTemplate: 'layout',
    loadingTemplate: 'loading',
    notFoundTemplate: 'notFound'
});

//Route to a single post thread
Router.route('/posts/:_id', {
    name: 'postPage',

    waitOn: function () {
        return [
      Meteor.subscribe('singlePost', this.params._id),
      Meteor.subscribe('comments', this.params._id)
    ];
    },

    //pass the data context (the current post) to the template
    data: function () {
        Session.set("currentDiscussion", this.params._id);
        return Posts.findOne(this.params._id);
    }
});

Router.route('/posts/:_id/edit', {
    name: 'postEdit',

    waitOn: function () {
        return Meteor.subscribe('singlePost', this.params._id);
    },

    data: function () {
        return Posts.findOne(this.params._id);
    }
});

Router.route('/submit', {
    name: 'postSubmit'
});

//Function adapted from the book Discover Meteor - Building Real-Time JavaScript Web Apps p.209
PostsListController = RouteController.extend({
    template: 'postsList',
    increment: 5,

    postsLimit: function () {
        return parseInt(this.params.postsLimit) || this.increment;
    },

    findOptions: function () {
        return {
            sort: {
                submitted: -1
            },
            limit: this.postsLimit()
        };
    },

    subscriptions: function () {
        this.postsSub = [Meteor.subscribe('images'),
                          Meteor.subscribe('posts', this.findOptions())]
    },

    posts: function () {

        //default searching date
        var start = new Date(2010, 1, 1);
        var end = new Date(new Date().getFullYear(), 12, 31);


        var yearToFiltyBy = Session.get("currentYear");

        if (yearToFiltyBy != null && yearToFiltyBy != '') {
            start = new Date(yearToFiltyBy, 1, 1, 0, 0, 0, 0);
            end = new Date(yearToFiltyBy, 12, 31, 0, 0, 0, 0);
        }


        var options = {
            $gte: start,
            $lt: end
        };

        return Posts.find({
            submitted: options
        }, this.findOptions());
    },

    data: function () {
        var hasMore = this.posts().count() === this.postsLimit();

        var nextPath = this.route.path({
            postsLimit: this.postsLimit() + this.increment
        });

        return {
            posts: this.posts(),
            ready: this.postsSub.ready,
            nextPath: hasMore ? nextPath : null
        };
    }
});


Router.route('/:postsLimit?', {
    name: 'postsList'
});

//uncomment if access denied template is showed instead
//of the loading screen while waiting for the user credential validation
var requireLogin = function () {
    if (!Meteor.user()) {
        // if (Meteor.loggingIn()) {
        //   this.render(this.loadingTemplate);
        //} else {
        this.render('accessDenied');
        //}
    } else {
        this.next();
    }
}

Router.onBeforeAction('dataNotFound', {
    only: 'postPage'
});
Router.onBeforeAction(requireLogin, {
    only: 'postSubmit'
});
