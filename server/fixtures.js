// Fixture data
/** 
if (Posts.find().count() === 0) {
    var now = new Date().getTime();

    // create two users
    var tylerId = Meteor.users.insert({
        profile: {
            name: 'Tyler Durden'
        }
    });

    var tyler = Meteor.users.findOne(tylerId);

    var frankId = Meteor.users.insert({
        profile: {
            name: 'Frank Sinatra'
        }
    });

    var frank = Meteor.users.findOne(frankId);

    var telescopeId = Posts.insert({
        title: 'Introducing Online School',
        userId: frank._id,
        author: frank.profile.name,
        material: 'Cras tempus scelerisque est placerat ultrices. Integer sed nunc at urna auctor vehicula. Donec quis enim ac nisl cursus molestie vitae non risus. Suspendisse ut malesuada dolor. Vivamus varius, mi sit amet dignissim dignissim, neque nibh tempor diam, sagittis mollis odio nisi vel leo. Pellentesque quis metus rutrum lorem fermentum facilisis. Quisque rutrum tristique est eu laoreet. Vestibulum vulputate dignissim tellus ut interdum. Vivamus sit amet est sit amet sem scelerisque condimentum sed eu mi. Nullam tortor sem, mollis vitae sodales in, finibus sit amet nisi. Maecenas justo odio, maximus at gravida eget, convallis in orci. Pellentesque ligula nulla, hendrerit eu facilisis a, blandit et massa. Aliquam luctus fermentum sagittis. Suspendisse egestas sem at feugiat faucibus. In erat nisl, congue quis hendrerit quis, vehicula ac nisl. Proin sit amet ligula egestas, ornare orci quis, fringilla turpis.',
        submitted: new Date(2014, 3, 1),
        commentsCount: 6
    });

    var comId = Comments.insert({
        parentCommentId: '',
        postId: telescopeId,
        userId: tyler._id,
        author: tyler.profile.name,
        submitted: new Date(now - 5 * 3600 * 1000),
        body: 'Curabitur lectus ligula, lacinia sit amet scelerisque quis, viverra rutrum dui. Proin pulvinar tincidunt mauris at pretium. Pellentesque volutpat mi quis enim molestie, eget blandit nibh scelerisque.'
    });

    Comments.insert({
        parentCommentId: '',
        postId: telescopeId,
        userId: tyler._id,
        author: tyler.profile.name,
        submitted: new Date(now - 5 * 3600 * 1000),
        body: 'Nunc rutrum lacinia tempor. Vestibulum nisi tellus, ornare sed varius sit amet, viverra eget elit. Fusce non lorem dictum, malesuada augue eu, molestie lorem. Sed blandit, magna sit amet gravida lobortis, ipsum tortor tempor nulla, a viverra velit tortor nec eros. In ultrices mollis magna. Proin molestie convallis consequat. Praesent mollis, quam id dapibus egestas, odio dui vulputate ex, a dapibus leo ex vel augue. Aliquam ac lorem est. Praesent eu nunc erat. Cras tempus malesuada accumsan.'
    });

    var comChildId = Comments.insert({
        parentCommentId: comId,
        postId: telescopeId,
        userId: frank._id,
        author: frank.profile.name,
        submitted: new Date(now - 3 * 3600 * 1000),
        body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin neque nisi, facilisis in finibus vel, semper et ante. Nam vitae neque vel eros varius facilisis vel vel magna. Proin et nulla eu lorem iaculis fermentum sodales vel massa. Duis faucibus aliquam nulla. Integer malesuada mi non orci luctus pulvinar. Donec est nunc, laoreet ut lorem ac, vehicula commodo libero. Fusce imperdiet justo vitae risus bibendum euismod. Praesent nec faucibus dui. Donec bibendum porta magna. Suspendisse et tempus libero. Aliquam sed efficitur augue. Vestibulum ante erat, auctor id metus ac, hendrerit sollicitudin dui. Sed quis nisl eget justo egestas posuere. Vivamus non hendrerit nibh. Maecenas viverra pulvinar mattis'
    });

    var comChildId2 = Comments.insert({
        parentCommentId: comId,
        postId: telescopeId,
        userId: frank._id,
        author: frank.profile.name,
        submitted: new Date(now - 3 * 3600 * 1000),
        body: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. !'
    });


    Comments.insert({
        parentCommentId: '',
        postId: telescopeId,
        userId: tyler._id,
        author: tyler.profile.name,
        submitted: new Date(now - 5 * 3600 * 1000),
        body: 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? '
    });

    Comments.insert({
        parentCommentId: comChildId,
        postId: telescopeId,
        userId: tyler._id,
        author: tyler.profile.name,
        submitted: new Date(now - 5 * 3600 * 1000),
        body: 'Sed posuere mauris id volutpat scelerisque. Mauris ut ipsum eget lorem consequat posuere at eget ipsum. Cras lacinia ante quis maximus volutpat. Fusce interdum, felis vitae aliquam molestie, tellus arcu egestas lacus, vel rutrum diam magna eget ligula. Morbi eu ligula imperdiet, pulvinar ipsum a, rutrum libero. Nullam quis ipsum eu odio rhoncus ultrices nec sed dolor. Vestibulum dapibus felis condimentum nisi lobortis pellentesque. Suspendisse condimentum scelerisque sapien at pretium. Maecenas condimentum ligula sed fermentum malesuada. Praesent a massa ullamcorper turpis condimentum dignissim. Donec molestie semper porttitor. Fusce lacinia mauris eu magna maximus, in interdum sapien tincidunt. Pellentesque eleifend risus sit amet lectus imperdiet efficitur. Proin vitae mi tincidunt, ultrices est at, varius leo. Praesent ac suscipit nibh.'
    });
}
*/